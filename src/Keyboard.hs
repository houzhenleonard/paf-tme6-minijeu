
module Keyboard where

import SDL

import Data.List (foldl')

import Data.Set (Set)
import qualified Data.Set as S
import Mouse(Mouse)
import qualified  Mouse as Mou

type Keyboard = Set Keycode

-- | création de la structure d'état de clavier (vide)
createKeyboard :: Keyboard
createKeyboard = S.empty

createMouse:: Mouse
createMouse = S.empty


--handleEvent :: Event -> Keyboard -> Keyboard
handleEvent :: Event -> (Keyboard, Mouse) -> (Keyboard, Mouse)
handleEvent event (kbd,mouse) =
  case eventPayload event of
    KeyboardEvent keyboardEvent ->
      if keyboardEventKeyMotion keyboardEvent == Pressed
      then ((S.insert (keysymKeycode (keyboardEventKeysym keyboardEvent)) kbd),mouse)
      else if keyboardEventKeyMotion keyboardEvent == Released
           then ((S.delete (keysymKeycode (keyboardEventKeysym keyboardEvent)) kbd),mouse)
           else (kbd,mouse)
    MouseButtonEvent mouseEvent ->
      if mouseButtonEventMotion mouseEvent == Pressed 
      then (kbd,(S.insert (mouseButtonEventButton mouseEvent) mouse))
      else if mouseButtonEventMotion  mouseEvent == Released 
            then (kbd,(S.delete (mouseButtonEventButton mouseEvent) mouse))
            else (kbd,mouse)
    _ -> (kbd,mouse)

-- | prise en compte des événements SDL2 pour mettre à jour l'état du clavier
--handleEvents :: [Event] -> Keyboard -> Keyboard
handleEvents :: [Event] -> (Keyboard, Mouse) ->  (Keyboard, Mouse)
handleEvents events (kbd,mouse) = foldl' (flip handleEvent) (kbd,mouse) events





-- | quelques noms de *keycode*
keycodeName :: Keycode -> Char
keycodeName KeycodeA = 'a'
keycodeName KeycodeB = 'b'
keycodeName KeycodeC = 'c'
keycodeName KeycodeD = 'd'
keycodeName KeycodeE = 'e'
keycodeName KeycodeF = 'f'
keycodeName KeycodeG = 'g'
keycodeName KeycodeH = 'h'
keycodeName KeycodeI = 'i'
keycodeName KeycodeJ = 'j'
keycodeName KeycodeK = 'k'
keycodeName KeycodeL = 'l'
keycodeName KeycodeM = 'm'
keycodeName KeycodeN = 'n'
keycodeName KeycodeO = 'o'
keycodeName KeycodeP = 'p'
keycodeName KeycodeQ = 'q'
keycodeName KeycodeR = 'r'
keycodeName KeycodeS = 's'
keycodeName KeycodeT = 't'
keycodeName KeycodeU = 'u'
keycodeName KeycodeV = 'v'
keycodeName KeycodeW = 'w'
keycodeName KeycodeX = 'x'
keycodeName KeycodeY = 'y'
keycodeName KeycodeZ = 'z'
keycodeName _ = '-'

-- | Vérifies sir le *keycode* spécificé est actuellement
-- | actif sur le clavier.
keypressed :: Keycode -> Keyboard -> Bool
keypressed kc kbd = S.member kc kbd


mousepressed :: MouseButton -> Mouse -> Bool
mousepressed mc kbd = S.member mc kbd