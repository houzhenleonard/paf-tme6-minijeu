module Mouse where

import SDL

import Data.List (foldl')

import Data.Set (Set)
import qualified Data.Set as S

type Mouse = Set MouseButton 

-- | création de la structure d'état de clavier (vide)
createMouse:: Mouse
createMouse = S.empty

getMouseLocation :: Event -> Maybe (V2 Int)
getMouseLocation event =
    case eventPayload event of
        MouseButtonEvent mouseButton ->
            let P coordinates = fmap fromIntegral (mouseButtonEventPos mouseButton) in
                Just coordinates
        _ -> Nothing


mousepressed :: MouseButton -> Mouse -> Bool
mousepressed mc kbd = S.member mc kbd