
module Model where

import SDL

import Keyboard (Keyboard)
import qualified Keyboard as K
import Mouse
import qualified Mouse as M
data GameState = GameState { persoX :: Int
                           , persoY :: Int
                           , speed :: Int }
  deriving (Show)


initGameState :: GameState
initGameState = GameState 200 300 4

moveLeft :: GameState -> GameState
moveLeft  g@(GameState x   y  s )
          | x > 0 =  (GameState (x-s)   y  s )
          | otherwise =  g
 
moveRight :: GameState -> GameState
moveRight g@(GameState x   y  s )
          | x <540 =  (GameState (x+s)   y  s )
          | otherwise =  g
                              
moveUp :: GameState -> GameState
moveUp gs@(GameState x y s) 
        | y > 0 = (GameState x (y-s) s)
        |otherwise  = gs

moveDown :: GameState -> GameState
moveDown gs@(GameState x y s) 
        | y  < 380= (GameState x (y+s) s)
        |otherwise  = gs

mouseTouche :: GameState -> GameState
mouseTouche gs@(GameState x y s) 
        | y  < 380= do (GameState x (y+s) s)
        |otherwise  = gs

gameStep :: RealFrac a => GameState -> (Keyboard, Mouse) -> a -> GameState
gameStep gstate (kbd,mou) deltaTime =
  let modif = (if K.keypressed KeycodeLeft kbd
               then moveLeft else id)
              .
              (if K.keypressed KeycodeRight kbd
               then moveRight else id)
              .
              (if K.keypressed KeycodeUp kbd
               then moveUp else id)
              .
              (if K.keypressed KeycodeDown kbd
               then moveDown else id)
              .
              (if M.mousepressed ButtonLeft mou
                then (do
                  --(putStrLn ("Touché !") )
                  mouseTouche) else id )

  in modif gstate

insideGameState :: Maybe (V2 Int) -> GameState ->Bool 
insideGameState coord gs@(GameState px py _)=
  case coord of
    Just(V2 x y) -> px<=x && x<=(px+100) && py<=y &&y<=(py+100)
    Nothing  ->False

